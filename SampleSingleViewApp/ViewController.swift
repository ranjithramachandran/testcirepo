//
//  ViewController.swift
//  SampleSingleViewApp
//
//  Created by Ramachandran, Ranjith (ETW) on 1/9/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        label.accessibilityIdentifier = "Label"
        button.accessibilityIdentifier = "Button"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func buttonPressed(_ sender: Any) {
        label.text = "Hello"
        label.backgroundColor = .red
    }
}

